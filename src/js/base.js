var jsArrayNeedLoad = ["./base/jquery.min.js",
	"./base/bootstrap/js/bootstrap.min.js",
	//	"./base/materialize/js/materialize.min.js",
	"./base/vue.js",
	"./base/string.js",
	"./base/moment.js",
	"./base/layer/layer.js",
	"./base/bootstrapvalidator/js/bootstrapValidator.change.js",
	//	"./base/bootstrapvalidator/js/bootstrapValidator.min.js"

	"./base/bootstrapvalidator/js/language/zh_CN.js"
];

function loadJs(src, callback) {
	var script = document.createElement('script'),
		body = document.getElementsByTagName('body')[0];
	script.type = 'text/javascript';
	script.charset = 'UTF-8';
	script.src = src;
	if(script.addEventListener) {
		script.addEventListener('load', function() {
			if(!!callback) {
				callback();
			}
		}, false);
	} else if(script.attachEvent) {
		script.attachEvent('onreadystatechange', function() {
			var target = window.event.srcElement;
			if(target.readyState == 'loaded') {
				callback();
			}
		});
	}
	body.appendChild(script);
}

function loadCss(src, callback) {
	var link = document.createElement('link'),
		head = document.getElementsByTagName('head')[0];
	link.rel = "stylesheet";
	link.href = src;
	if(link.addEventListener) {
		link.addEventListener('load', function() {
			if(!!callback) {
				callback();
			}
		}, false);
	} else if(link.attachEvent) {
		link.attachEvent('onreadystatechange', function() {
			var target = window.event.srcElement;
			if(link.readyState == 'loaded') {
				callback();
			}
		});
	}
	head.appendChild(link);
}

//function loadAllJS(i) {
//	if(i < jsArrayNeedLoad.length) {
//		var next = i + 1;
//		loadJs(jsArrayNeedLoad[i], function() {
//			if(next < jsArrayNeedLoad.length) {
//				loadAllJS(next);
//			} else {
//				loadPageJs();
//			}
//		})
//	}
//}

function loadArrayJs(arr, callback) {
	loadByIndex(arr, 0, callback);
}

function loadByIndex(arr, i, callback) {
	if(i < arr.length) {
		var next = i + 1;
		loadJs(arr[i], function() {
			if(next < arr.length) {
				loadByIndex(arr, next, callback);
			} else {
				if(!!callback) {
					callback();
				}
			}
		})
	}
}
var path = location.pathname;
var as = path.split("/");
var name = as.pop().replace(".html", "");
var nameInSet = document.getElementById("serverDataPagename").innerText;
if(nameInSet.length > 0) {
	name = nameInSet;
}
jsArrayNeedLoad.push("./js/" + name + ".js");
//优先加载css
loadCss("./css/" + name + ".css", function() {
	//	loadAllJS(0);
	loadArrayJs(jsArrayNeedLoad);
})

function loadPageJs() {
	loadJs("./js/" + name + ".js");
}