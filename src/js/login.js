var jsArray = ["./base/materialize/js/materialize.min.js",
	"./widget/btnanimation/dist/ladda.jquery.min.js",
	"./widget/btnanimation/dist/spin.min.js",
	"./widget/btnanimation/dist/ladda.min.js"

];
var ladda;
loadArrayJs(jsArray, function() {
		ladda = Ladda.create(document.querySelector('#loginbtn'));

})

function login() {
	var account = $("#account").val();
	var password = $("#password").val();
	if(S(account).isEmpty() || S(password).isEmpty()) {
		layer.msg("用户名和密码不能为空！");
		return;
	}
	ladda.start();
	$.ajax({
		url: "./loginpost",
		type: "post",
		data: {
			account: account,
			password: password
		},
		success: function(data, textStatus, xhr) {
			console.log(data);
			if(data.success) {
				location.href = data.message;
			} else {
				layer.msg(data.message);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			layer.msg(textStatus);
		},
		complete: function() {
			ladda.stop();
		}
	})
}