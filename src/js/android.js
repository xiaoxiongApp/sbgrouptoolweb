var jsArray = ["./widget/My97DatePicker/WdatePicker.js",
	"./widget/jqgrid/js/jquery.jqGrid.min.js",
	"./widget/jqgrid/js/i18n/grid.locale-cn.js",
	"./widget/fileUpload/js/plugins/sortable.js",
	"./widget/fileUpload/js/fileinput.js",
	"./widget/fileUpload/js/locales/zh.js",
	"./widget/fileUpload/themes/explorer/theme.js"
];

//  <script src="../js/plugins/sortable.js" type="text/javascript"></script>
//  <script src="../js/fileinput.js" type="text/javascript"></script>
//  <script src="../js/locales/fr.js" type="text/javascript"></script>
//  <script src="../js/locales/es.js" type="text/javascript"></script>
//  <script src="../themes/explorer/theme.js" type="text/javascript"></script>
loadArrayJs(jsArray, function() {
	var grid = $("#jqGrid").jqGrid({
		url: './androidQuery',
		//		editurl: "./androidEdit",
		pagerpos: "left",
		mtype: "POST",
		styleUI: 'Bootstrap',
		datatype: "json",
		pgtext: "{0}/{1}",
		recordtext: "显示{0}~{1}",
		jsonReader: {
			rows: "rows",
			page: "page",
			total: "total",
			records: "records"
		},
		colModel: [{
			label: '产品',
			name: 'ductid',
			width: 150,
			formatter: function(cellValue) {
				return '产品';
			},
			align: 'center',
			sortable: false
		}, {
			label: '项目名称',
			name: 'name',
			key: true,
			width: 75,
			align: 'center',
			sortable: false
		}, {
			label: '基础安装包',
			name: 'apkpath',
			width: 150,
			formatter: function(cellValue) {
				if(!cellValue) {
					return "否";
				}
				return cellValue.length > 0 ? "是" : "否";
			},
			align: 'center',
			sortable: false
		}],
		postData: {
			ductid: !!pageData && pageData.length > 0 ? pageData[0].id : "0"
		},
		page: 1, //设置初始的页码
		records: 10,
		viewrecords: true,
		rowNum: 30, //在grid上显示记录条数，这个参数是要被传递到后台
		autoheight: true, //自动拉伸高度
		autowidth: true, //自动拉伸宽度
		pager: "#jqGridPager",
		onSelectRow: function(rowid) {
			setRightArea(grid.getRowData(rowid));
		}
	});

	var projctVue = new Vue({
		el: "#query-product",
		data: {
			items: pageData
		},
		methods: {
			changeValue: function(ele) {
				console.log(arguments)
				console.log(ele);
				var optionTxt = $(ele.target).find("option:selected").text();
				var optionVal = $(ele.target).find("option:selected").val();
				grid.setGridParam({
					postData: {
						ductid: optionVal
					}
				}, true).trigger('reloadGrid');
			}
		}
	});
	var rightUpload = new ApkUploadRight();
	var propertyVue = new Vue({
		el: "#proarea",
		data: {
			hasApk: false
		}
	});

	function setRightArea(rowData) {
		console.log(rowData);
	}
	$("#content").show();
	resetSize();
	$(window).resize(function() {
		resetSize();
	});
});

//右侧的上传文件类
function ApkUploadRight() {
	var uploadData = {};
	var uploadObject = $("#apkFileInRight").fileinput({
		uploadUrl: './uploadOnlyApk', // you must set a valid URL here else you will get an error
		allowedFileExtensions: ['apk'],
		showCaption: true,
		showPreview: true,
		showRemove: false,
		showUpload: true,
		showCancel: false,
		uploadExtraData: function() {
			return uploadData;
		},
		fileType: "apk",
		overwriteInitial: false,
		maxFileSize: 1000,
		maxFilesNum: 1,
		//allowedFileTypes: ['image', 'video', 'flash'],
		slugCallback: function(filename) {
			return filename.replace('(', '_').replace(']', '_');
		}
	}).on('fileuploaderror', function(e, params) {
		console.log('fileuploaderror', e, params);
	}).on('filepreajax', function(event, previewId, index) { //上传前处理
	}).on('fileuploaded', function(e, params) {
		console.log('文件上传成功');
	});
	this.upload = function(projectId) {
		if(!!projectId) {
			return;
		}
		uploadData = {
			projectId: projectId
		};
		uploadObject.fileinput("upload");
	}
	this.setProjectId = function(projectId) {
		if(!!projectId) {
			return;
		}
		uploadData = {
			projectId: projectId
		};
	}
}

function resetSize() {
	$("#jqGrid").jqGrid().setGridWidth($(".part-list").width() - 10)
	$("#jqGrid").jqGrid().setGridHeight($(".part-list").height() - 60)
}

$("#btn-search").on("click", btnSearch);
$("#btn-add").on("click", btnAdd);

function btnSearch() {

}

var html = $("#addTablediv").html();
$("#addTablediv").remove();

function btnAdd() {
	var layerIndex = layer.open({
		type: 1,
		title: "新增项目",
		closeBtn: 1,
		shadeClose: true,
		//	skin: 'yourclass',
		area: ['600px', '400px'],
		content: html,
		btn: ['确认', '取消'],
		yes: function(index, layero) {
			$('#addTable').data('bootstrapValidator').validate();
			if(!$('#addTable').data('bootstrapValidator').isValid()) {
				return;
			}
			var duckid = $("#productlist").find("option:selected").val();
			var duckName = duckid == -1 ? $("#newProductName").val() : "";
			var projectName = $("#newProjectName").val();
			uploadData = {
				duckid: duckid,
				duckName: duckName,
				projectName: projectName
			};
			if($("#apkFile").val().length > 0) {
				uploadObject.fileinput("upload");
			} else {
				$.ajax({
					url: "./createProject",
					data: uploadData,
					type: "post",
					dataType: "json",
					success: function(data, status, xhr) {
						layer.close(index);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						layer.msg(textStatus);
					}
				})
			}

		},
		btn2: function(index, layero) {
			//按钮【按钮三】的回调
			console.log("按钮");
			console.log(index)
			console.log(layero)
			return false;
			//return false 开启该代码可禁止点击该按钮关闭
		}
	});
	var tempData = $.extend([], pageData, true);
	tempData.unshift({
		"name": "新建",
		id: "-1"
	});
	console.log(tempData);
	var vuep = new Vue({
		el: "#productlist",
		data: {
			items: tempData
		},
		methods: {
			changeValue: function(ele) {
				var optionTxt = $(ele.target).find("option:selected").text();
				var optionVal = $(ele.target).find("option:selected").val();
				if(optionVal == "-1") {
					$('#addTable').data('bootstrapValidator').addField("newProductName", newProductNameOption);
					$("#newProductNameDiv").show();
				} else {
					$('#addTable').data('bootstrapValidator').removeField("newProductName");
					$("#newProductNameDiv").hide();
				}
			}
		}
	});
	var newProductNameOption = {
		message: 'The newProductName is not valid',
		validators: {
			notEmpty: {
				message: ''
			}
		}
	}
	var tableValidator = $('#addTable').bootstrapValidator({
		message: '无效的参数',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			newProductName: newProductNameOption,
			newProjectName: {
				message: 'The newProjectName is not valid',
				validators: {
					notEmpty: {
						message: '项目名称不能为空！'
					}
				}
			}
		}
	});
	var uploadData = {};
	var uploadObject = $("#apkFile").fileinput({
		uploadUrl: './upload', // you must set a valid URL here else you will get an error
		allowedFileExtensions: ['apk'],
		showCaption: true,
		showPreview: false,
		showRemove: false,
		showUpload: false,
		showCancel: false,
		uploadExtraData: function() {
			return uploadData;
		},
		fileType: "apk",
		overwriteInitial: false,
		maxFileSize: 1000,
		maxFilesNum: 1,
		//allowedFileTypes: ['image', 'video', 'flash'],
		slugCallback: function(filename) {
			return filename.replace('(', '_').replace(']', '_');
		}
	}).on('fileuploaderror', function(e, params) {
		console.log('fileuploaderror', e, params);
	}).on('filepreajax', function(event, previewId, index) { //上传前处理
	}).on('fileuploaded', function(e, params) {
		console.log('文件上传成功');
		layer.close(layerIndex); //如果设定了yes回调，需进行手工关闭
	});

}