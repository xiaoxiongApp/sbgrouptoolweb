var jsArray = ["./widget/My97DatePicker/WdatePicker.js",
	"./widget/jqgrid/js/jquery.jqGrid.min.js",
	"./widget/jqgrid/js/i18n/grid.locale-cn.js"
];
loadArrayJs(jsArray, function() {
	$(function() {
		initDateIime();
		var grid = $("#jqGrid").jqGrid({
			url: 'http://trirand.com/blog/phpjqgrid/examples/jsonp/getjsonp.php?callback=?&qwery=longorders',
			mtype: "GET",
			styleUI: 'Bootstrap',
			datatype: "jsonp",
			colModel: [{
					label: 'OrderID',
					name: 'OrderID',
					key: true,
					width: 75
				},
				{
					label: 'Customer ID',
					name: 'CustomerID',
					width: 150
				},
				{
					label: 'Order Date',
					name: 'OrderDate',
					width: 150
				},
				{
					label: 'Freight',
					name: 'Freight',
					width: 150
				},
				{
					label: 'Ship Name',
					name: 'ShipName',
					width: 150
				}
			],
			viewrecords: true,
			rowNum: 50,
			autoheight: true, //自动拉伸高度
			autowidth: true, //自动拉伸宽度
			pager: "#jqGridPager"
		});
		resetSize();
		$(window).resize(function() {
			resetSize();
		});
		$("#btn-search").on("click", btnSearch);
		$("#btn-add").on("click", btnAdd);
	});
});

function resetSize() {
	$("#jqGrid").jqGrid().setGridWidth($(".part-list").width() - 10)
	$("#jqGrid").jqGrid().setGridHeight($(".part-list").height() - 60)
}

function initDateIime() {
	var now = moment();
	var formatString = "YYYY-MM-DD HH:mm:ss";
	var beginTimeString = moment().subtract(3, 'days').format(formatString);
	var endTimeString = now.format(formatString);
	$("#beginTime").val(beginTimeString);
	$("#endTime").val(endTimeString);
}

function btnSearch() {
  //top.location.href = "login.html";
}

function btnAdd() {
	layer.open({
		type: 2,
		title: '新增报修',
		shadeClose: true,
		shade: 0.5,
		maxmin: true, //开启最大化最小化按钮
		area: ['893px', '600px'],
		content: './publicRentalHousingForm.html'
	});
}

var queryorderstatus = [{
	"text": "已回访",
	"value": "1"
}, {
	"text": "未回访",
	"value": "2"
}, {
	"text": "已结算",
	"value": "3"
}, {
	"text": "未结算",
	"value": "4"
}, {
	"text": "已完工",
	"value": "5"
}, {
	"text": "未完工",
	"value": "5"
}];
var vue_queryorderstatus = new Vue({
	el: "#query-orderstatus",
	data: {
		items: queryorderstatus
	}
});